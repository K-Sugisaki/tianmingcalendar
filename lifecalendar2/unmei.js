import moment from 'moment';

/**
 * 運命サービス
 */
export function UnmeiService () {
	"use strict";
	//console.log('=== UnmeiService');
	// 初期値
	var service = [];
	service.year	= 2001;
	service.month = 0; // 0~11
	service.date	= 1;
	if (global.locale == 'ja-JP' || global.locale.match(/zh/)) {
		service.juunisiTable =
			["子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"];
	} else {
		service.juunisiTable =
			["Rat","Ox","Tiger","Rabbit","Dragon","Snake","Horse","Sheep","Monkey","Rooster","Dog","Boar"];
	}		 

	/**
	 * setDate
	 * 生年月日を設定して、天中殺の十二支を返す
	 *
	 * @param {Array} i_date: 日付の配列
	 * @return {Array} 例: ['子','', '丑']
	 */
	function setDate(i_date) {
		service.year	= i_date[0];
		service.month = i_date[1];
		service.date	= i_date[2];
		
		getUnmeiNum();
		getEtoNum();
		var num = getTenchusatuNum();
		var tenchusatuTable = tenchusatuTableData();
		var tenchusatu = tenchusatuTable[num];
		return tenchusatu;
	}
	function getUnmeiNum() { // 運命数を出す
		//console.log('--- getUnmeiNum()');
		var unmeiTable = unmeiTableData();
		if (unmeiTable[service.year] !== undefined) {
			var unmeiNum = unmeiTable[service.year][service.month];
			return unmeiNum;
		} else {
			//console.log('Error: The year is out of range. year: ' + service.year);
			return null;
		}
	}
	function getEtoNum() { // 干支番号を算出
		//console.log('--- getEtoNum()');
		var unmeiNum = getUnmeiNum();
		var sum = unmeiNum + service.date;
		var etoNum;
		if (sum > 60) {
			etoNum = sum - 60 - 1;
		} else {
			etoNum = sum - 1;
		}
		return etoNum;
	}
	function getTenchusatuNum() { // 天中殺の番号（０〜５）を返す
		//console.log('--- getTenchusatuNum()');
		var etoNum = getEtoNum();
		var tenchusatuNum = null;
		if (etoNum >= 50)			 {tenchusatuNum = 0;}
		else if (etoNum >= 40) {tenchusatuNum = 1;}
		else if (etoNum >= 30) {tenchusatuNum = 2;}
		else if (etoNum >= 20) {tenchusatuNum = 3;}
		else if (etoNum >= 10) {tenchusatuNum = 4;}
		else if (etoNum >=	0) {tenchusatuNum = 5;}
		return tenchusatuNum;
	}

	return {
		setBirthDate: function (i_date) { // 生年月日を設定する
			//console.log('### UnmeiService.setBirthDate()');
			return setDate(i_date);
		},
		setBirthday: function (i_day) { // 生年月日を設定する
			//console.log('### UnmeiService.setBirthday()');
			var year	= i_day.year();
			var month = i_day.month(); // 0~11
			var date	= i_day.date();
			return setDate([year, month, date]);
		},
		getEtoNum: function () { // 干支番号
			return getEtoNum();
		},
		getTenchusatu: function () { // 天中殺の判定結果
			var num = getTenchusatuNum();
			var tenchusatuTable = tenchusatuTableData();
			var tenchusatu = tenchusatuTable[num];
			return tenchusatu;
		},
		getTenchusatuMonth: function (separator = null) { // 天中殺の月
			var num = getTenchusatuNum();
			var tenchusatuMonthTable = tenchusatuMonthTableData();
			var month = tenchusatuMonthTable[num];
			if (separator === null) {
				return month;
			} else {
				if (global.locale == 'ja-JP' || global.locale.match(/zh/)) {
					return month[0] +'月'+ separator + month[1] +'月';
				} else {
					let monthNames = ["January", "February", "March", "April", "May", "June",
														"July", "August", "September", "October", "November", "December"];
					return monthNames[month[0] -1] + separator + monthNames[month[1] -1];
				}
			}
		}
	};
	
	function tenchusatuTableData() {
		if (global.locale == 'ja-JP' || global.locale.match(/zh/))
			var separator = '';
		else
			var separator = ', ';
		return [[service.juunisiTable[0], separator, service.juunisiTable[1]],
						[service.juunisiTable[2], separator, service.juunisiTable[3]],
						[service.juunisiTable[4], separator, service.juunisiTable[5]],
						[service.juunisiTable[6], separator, service.juunisiTable[7]],
						[service.juunisiTable[8], separator, service.juunisiTable[9]],
						[service.juunisiTable[10], separator, service.juunisiTable[11]]
					 ];
	}
	function tenchusatuMonthTableData() {
		return [[12,1], [2,3], [4,5], [6,7], [8, 9], [10, 11]];
	}
	function unmeiTableData() {
			var unmeiTable = new Array();
			
			unmeiTable[1926] = [26, 57, 25, 56, 26, 57, 27, 58, 29, 59, 30, 0];
			unmeiTable[1927] = [31, 2, 30, 1, 31, 2, 32, 3, 34, 4, 35, 5];
			unmeiTable[1928] = [36, 7, 36, 7, 37, 8, 38, 39, 40, 10, 41, 11];
			unmeiTable[1929] = [42, 13, 41, 12, 42, 13, 43, 14, 45, 15, 46, 16];
			unmeiTable[1930] = [47, 18, 46, 17, 47, 18, 48, 19, 50, 20, 51, 21];
			unmeiTable[1931] = [52, 23, 51, 22, 52, 23, 53, 24, 55, 25, 56, 26];
			unmeiTable[1932] = [57, 28, 57, 28, 58, 29, 59, 30, 1, 31, 2, 32];
			unmeiTable[1933] = [3, 34, 2, 33, 3, 34, 4, 35, 6, 36, 7, 37];
			unmeiTable[1934] = [8, 39, 7, 38, 8, 39, 9, 40, 11, 41, 12, 42];
			unmeiTable[1935] = [13, 44, 12, 43, 13, 44, 14, 45, 16, 46, 17, 47];
			unmeiTable[1936] = [18, 49, 18, 49, 19, 50, 20, 51, 22, 52, 23, 53];
			unmeiTable[1937] = [24, 55, 23, 54, 24, 55, 25, 56, 27, 57, 28, 58];
			unmeiTable[1938] = [29, 0, 28, 59, 29, 0, 30, 1, 32, 2, 33, 3];
			unmeiTable[1939] = [34, 5, 33, 4, 34, 5, 35, 6, 37, 7, 38, 8];
			unmeiTable[1940] = [39, 10, 39, 10, 40, 11, 41, 12, 43, 13, 44, 14];
			unmeiTable[1941] = [45, 16, 44, 15, 45, 16, 46, 17, 48, 18, 49, 19];
			unmeiTable[1942] = [50, 21, 49, 20, 50, 21, 51, 22, 53, 23, 54, 24];
			unmeiTable[1943] = [55, 26, 54, 25, 55, 26, 56, 27, 58, 28, 59, 29];
			unmeiTable[1944] = [0, 31, 0, 31, 1, 32, 2, 33, 4, 34, 5, 35];
			unmeiTable[1945] = [6, 37, 5, 36, 6, 37, 7, 38, 9, 39, 10, 40];
			unmeiTable[1946] = [11, 42, 10, 41, 11, 42, 12, 43, 14, 44, 15, 45];
			unmeiTable[1947] = [16, 47, 15, 46, 16, 47, 17, 48, 19, 49, 20, 50];
			unmeiTable[1948] = [21, 52, 21, 52, 22, 53, 23, 54, 25, 55, 26, 56];
			unmeiTable[1949] = [27, 58, 26, 57, 27, 58, 28, 59, 30, 0, 31, 1];
			unmeiTable[1950] = [32, 3, 31, 2, 32, 3, 33, 4, 35, 5, 36, 6];
			unmeiTable[1951] = [37, 8, 36, 7, 37, 8, 38, 9, 40, 10, 41, 11];
			unmeiTable[1952] = [42, 13, 42, 13, 43, 14, 44, 15, 46, 16, 47, 17];
			unmeiTable[1953] = [48, 19, 47, 18, 48, 19, 49, 20, 51, 21, 52, 22];
			unmeiTable[1954] = [53, 24, 52, 23, 53, 24, 54, 25, 56, 26, 57, 27];
			unmeiTable[1955] = [58, 29, 57, 28, 58, 29, 59, 30, 1, 31, 2, 32];
			unmeiTable[1956] = [3, 34, 3, 34, 4, 35, 5, 36, 7, 37, 8, 38];
			unmeiTable[1957] = [9, 40, 8, 39, 9, 40, 10, 41, 12, 42, 13, 43];
			unmeiTable[1958] = [14, 45, 13, 44, 14, 45, 15, 46, 17, 47, 18, 48];
			unmeiTable[1959] = [19, 50, 18, 49, 19, 50, 20, 51, 22, 52, 23, 53];
			unmeiTable[1960] = [24, 55, 24, 55, 25, 56, 26, 57, 28, 58, 29, 59];
			unmeiTable[1961] = [30, 1, 29, 0, 30, 1, 31, 2, 33, 3, 34, 4];
			unmeiTable[1962] = [35, 6, 34, 5, 35, 6, 36, 7, 38, 8, 39, 9];
			unmeiTable[1963] = [40, 11, 39, 10, 40, 11, 41, 12, 43, 13, 44, 14];
			unmeiTable[1964] = [45, 16, 45, 16, 46, 17, 47, 18, 49, 19, 50, 20];
			unmeiTable[1965] = [51, 22, 50, 21, 51, 22, 52, 23, 54, 24, 55, 25];
			unmeiTable[1966] = [56, 27, 55, 26, 56, 27, 57, 28, 59, 29, 0, 30];
			unmeiTable[1967] = [1, 32, 0, 31, 1, 32, 2, 33, 4, 34, 5, 35];
			unmeiTable[1968] = [6, 37, 6, 37, 7, 38, 8, 39, 10, 40, 11, 41];
			unmeiTable[1969] = [12, 43, 11, 42, 12, 43, 13, 44, 15, 45, 16, 46];
			unmeiTable[1970] = [17, 48, 16, 47, 17, 48, 18, 49, 20, 50, 21, 51];
			unmeiTable[1971] = [22, 53, 21, 52, 22, 53, 23, 54, 25, 55, 26, 56];
			unmeiTable[1972] = [27, 58, 27, 58, 28, 59, 29, 0, 31, 1, 32, 2];
			unmeiTable[1973] = [33, 4, 32, 3, 33, 4, 34, 5, 36, 6, 37, 7];
			unmeiTable[1974] = [38, 9, 37, 8, 38, 9, 39, 10, 41, 11, 42, 12];
			unmeiTable[1975] = [43, 14, 42, 13, 43, 14, 44, 15, 46, 16, 47, 17];
			unmeiTable[1976] = [48, 19, 48, 19, 49, 20, 50, 21, 52, 22, 53, 23];
			unmeiTable[1977] = [54, 25, 53, 24, 54, 25, 55, 26, 57, 27, 58, 28];
			unmeiTable[1978] = [59, 30, 58, 29, 59, 30, 0, 31, 2, 32, 3, 33];
			unmeiTable[1979] = [4, 35, 3, 34, 4, 35, 5, 36, 7, 37, 8, 38];
			unmeiTable[1980] = [9, 40, 9, 40, 10, 41, 11, 42, 13, 43, 14, 44];
			unmeiTable[1981] = [15, 46, 14, 45, 15, 46, 16, 47, 18, 48, 19, 49];
			unmeiTable[1982] = [20, 51, 19, 50, 20, 51, 21, 52, 23, 53, 24, 54];
			unmeiTable[1983] = [25, 56, 24, 55, 25, 56, 26, 57, 28, 58, 29, 59];
			unmeiTable[1984] = [30, 1, 30, 1, 31, 2, 32, 3, 34, 4, 35, 5];
			unmeiTable[1985] = [36, 7, 35, 6, 36, 7, 37, 8, 39, 9, 40, 10];
			unmeiTable[1986] = [41, 12, 40, 11, 41, 12, 42, 13, 44, 14, 45, 15];
			unmeiTable[1987] = [46, 17, 45, 16, 46, 17, 47, 18, 49, 19, 50, 20];
			unmeiTable[1988] = [51, 22, 51, 22, 52, 23, 53, 24, 55, 25, 56, 26];
			unmeiTable[1989] = [57, 28, 56, 27, 57, 28, 58, 29, 0, 30, 1, 31];
			unmeiTable[1990] = [2, 33, 1, 32, 2, 33, 3, 34, 5, 35, 6, 36];
			unmeiTable[1991] = [7, 38, 6, 37, 7, 38, 8, 39, 10, 40, 11, 41];
			unmeiTable[1992] = [12, 43, 12, 43, 13, 44, 14, 45, 16, 47, 17, 47];
			unmeiTable[1993] = [18, 49, 17, 48, 18, 49, 19, 50, 21, 51, 22, 52];
			unmeiTable[1994] = [23, 54, 22, 53, 23, 54, 24, 55, 26, 56, 27, 57];
			unmeiTable[1995] = [28, 59, 27, 58, 28, 59, 29, 0, 31, 1, 32, 2];
			unmeiTable[1996] = [33, 4, 33, 4, 34, 5, 35, 6, 37, 7, 38, 8];
			unmeiTable[1997] = [39, 10, 38, 9, 39, 10, 40, 11, 42, 12, 43, 13];
			unmeiTable[1998] = [44, 15, 43, 14, 44, 15, 45, 16, 47, 17, 48, 18];
			unmeiTable[1999] = [49, 20, 48, 19, 49, 20, 50, 21, 52, 22, 53, 23];
			unmeiTable[2000] = [54, 25, 54, 25, 55, 26, 56, 27, 58, 28, 59, 29];
			unmeiTable[2001] = [0, 31, 59, 30, 0, 31, 1, 32, 3, 33, 4, 34];
			unmeiTable[2002] = [5, 36, 4, 35, 5, 36, 6, 37, 8, 38, 9, 39];
			unmeiTable[2003] = [10, 41, 9, 40, 10, 41, 11, 42, 13, 43, 14, 44];
			unmeiTable[2004] = [15, 46, 15, 46, 16, 47, 17, 48, 19, 49, 20, 50];
			unmeiTable[2005] = [21, 52, 20, 51, 21, 52, 22, 53, 24, 54, 25, 55];
			unmeiTable[2006] = [25, 57, 25, 56, 26, 57, 27, 58, 29, 59, 30, 53];
			unmeiTable[2007] = [30, 2, 30, 1, 31, 2, 32, 3, 34, 4, 35, 5];
			unmeiTable[2008] = [37, 7, 36, 7, 37, 8, 38, 9, 40, 10, 41, 11];
			
			return unmeiTable;
		}
}
