import moment from 'moment';

/**
 * 干支サービス
 */
export function EtoService () {
    "use strict";
    if (global.locale == 'ja-JP' || global.locale.match(/zh/)) {
        var jikkanTable  = ["甲","乙","丙","丁","戊","己","庚","辛","壬","癸"];
        var juunisiTable = ["子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"];
    } else {
        var jikkanTable  = [
                'wood-elder', 'wood-younger',
                'fire-elder', 'fire-younger',
                'earth-elder', 'earth-younger',
                'metal-elder', 'metal-younger',
                'water-elder', 'water-younger'
        ];
        var juunisiTable =
            ["Rat","Ox","Tiger","Rabbit","Dragon","Snake","Horse","Sheep","Monkey","Rooster","Dog","Boar"];
    }
  function getEtoNum(i_day) {
    var kousiDay = moment.utc('1925-12-06'); //干支の基準日（甲子）
    var diffDays = i_day.diff(kousiDay, 'days');
    var eto = diffDays % 60;
    var kan = eto % 10;
    var si  = eto % 12;
    return [eto, kan, si];
  }
    
  return {
    getEtoNum: function (i_day) {
      return getEtoNum(i_day);
    },
    getEto: function (i_day) {
            var etoNumArray = getEtoNum(i_day);
            var kan = jikkanTable[etoNumArray[1]];
            var si  = juunisiTable[etoNumArray[2]];
            return [kan + si, kan, si];
    },
    numToEto: function (i_etoNum) {
            var kan = jikkanTable[i_etoNum % 10];
            var si  = juunisiTable[i_etoNum % 12];
            return kan + si;
    },
    /**
     * getSiData
     * 期間（一ヶ月）の十二支データを返す
     *
     * @param i_day: 期間（月）の初日
     *        i_days: 期間（月）の日数
     * @return {moment} [n].day: 日付
     *         {string} [n].si: 十二支
     */
        getSiData: function (i_day, i_days) {
            var si_data = [];
            var date = i_day.format('YYYYMMDD');
            for (var dd = 0; dd < i_days; dd++) {
                // momentオブジェクトを日を変えながら渡しても、それはコピーされない。
                // momentオブジェクトをその都度実装して渡す。
                var day = moment.utc(date, 'YYYYMMDD'); // 最初の日
                day.add(dd, 'days');
                si_data.push({
                    day: day,
                    si: this.getEto(day)[2]
                });
            }
            return si_data;
        }
  };
}
