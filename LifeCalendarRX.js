/**
 * LifeCalendar on Redux
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Button,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
// import Calendar from './react-native-calendar.0.12.2/Calendar';
import { Calendar, LocaleConfig } from './node_modules/react-native-calendars';
import MyDay from './custom/react-native-calendars.1.21.0/src/calendar/day/basic';

import TimerMixin from 'react-timer-mixin';
import moment from 'moment';

// 多言語対応
import * as Localization from 'expo-localization';
import I18n from 'ex-react-native-i18n';
global.locale = Localization.locale;
I18n.fallbacks = true;

// 天中殺と干支
import { EtoService } from './lifecalendar2/eto';
import { UnmeiService } from './lifecalendar2/unmei';
const Eto = EtoService();
const Unmei = UnmeiService();

// 通知
import { NotifService } from './Notif';
const Notif = NotifService();

// スワイプ
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

// バナー
import { BannerService } from './services/banner';
// 定数
import Setting from './setting.json';

/**
 * その月mmtの最初の日から最後の日までの十二支データをイベントとして返す
 */
function getMonthlyEtoEvents(mmt) {
  let eto_events = [];
  let start_moment = moment(mmt.startOf('month')); // 期間スタート
  let period_days = mmt.endOf('month').format('D'); // 期間の日数
  Eto.getSiData(start_moment, period_days).map((si_data) => {
    eto_events.push({
      date: si_data['day'].format('YYYY-MM-DD'),
      text: si_data['si'],
      markFlag: false
    });
  });
  return eto_events;
}
/**
 * イベントtextが運命ならサークルマークを付ける
 */
const getCircleOnUnmeiEvents = function (events, birthday) {
  let updated_events = [];
  // 運命から天中殺を取得
  Unmei.setBirthday(moment(birthday));
  let tenchusatu = Unmei.getTenchusatu();
  // 天中殺の日にサークルマーク
  events.map(event => {
    if (tenchusatu.includes(event.text)) {
      updated_events.push(Object.assign({},event,{ markFlag: true }));
    } else {
      updated_events.push(event);
    }
  });
  return updated_events;
};
/**
 * 月の十二支イベントと運命イベントを付ける
 */
const getMonthlyEvents = function (m, birthday) {
  let eto_events = getMonthlyEtoEvents(m);
  let events = getCircleOnUnmeiEvents(eto_events, birthday);
  return events;
};
const styles = StyleSheet.create({
  topContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});

/** 
 * 公開ファンクション
 * ３ヶ月分のイベントを返す
 */
export function getEvents(cal_moment, birthday) {
  let cal = moment(cal_moment);
  let events1 = getMonthlyEvents(cal.startOf('month').subtract(1,'months'), birthday); // 先月
  let events2 = getMonthlyEvents(cal.add(1,'months'), birthday); // 今月
  let events3 = getMonthlyEvents(cal.add(1,'months'), birthday); // 来月
  let events = [...events1, ...events2, ...events3];
  return events;
}
/**
 * 公開ファンクション
 * 天中殺情報ファンクション
 */
export function getUnmeiInfo(birthday) {
  Unmei.setBirthday(moment(birthday));
  let unmei_info = {
    month: Unmei.getTenchusatuMonth(', '),
    type: Unmei.getTenchusatu()
  };
  return unmei_info;
};
/**
 * 公開ファンクション
 * 未来のイベントについて、通知をスケジュールする
 * 前日朝８時に通知
 */
export function setNotif(events) {
  Notif.cancelAll(); // 通知をクリア
  I18n.translations = {
    'ja-JP': {
      notif_title: '天命カレンダー',
      notif_message: '明日から天中殺に入ります'
    },
    'zh-CN': {
      notif_title: '天命日历',
      notif_message: '从明天起的不吉利的日子'
    },
    'zh': {
      notif_title: '天命日曆',
      notif_message: '從明天起的不吉利的日子。'
    },
    'en': {
      notif_title: 'Tian Ming Calendar',
      notif_message: 'Tomorrow is the empty day.'
    }
  };
  events.some((event) => {
    if (new Date(event.date) > new Date()) {
      // 未来だけ、
      let juunisiTable = ["子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"];
      if (event.markFlag && (juunisiTable.indexOf(event.text)%2) == 0) {
        // 天中殺の１日目だけ、通知設定
        Notif.setSchedule(
          I18n.t('notif_title'),
          I18n.t('notif_message'),
          moment(event.date).subtract(24-8, 'hours').toDate() // 前日朝８時
        );
        return true;
      }
    }
  });
}

class LifeCalendarRX extends Component {
  constructor(props) {
    super(props);
    this.props.initEvents();
    this.state = {
      layout_info: [],
      enableChangingMonth: true // 月の変更受付を許す
    };
    this.changeDates = [];
    this.setMonthEvents = this.setMonthEvents.bind(this);
    this.onSwipeChange = this.onSwipeChange.bind(this);
    this.onSwipeLeft = this.onSwipeLeft.bind(this);
    this.onSwipeRight = this.onSwipeRight.bind(this);
  }

  // 日付の月と、その前後numヶ月のイベントを設定する
  setMonthEvents(date, num = 1) {
      let direction = (num > 0)?1:-1;
      // 逆方向の月
      let mm = moment(date.dateString).add(- direction,'month');
      let events = getMonthlyEvents(mm, this.props.birthday);
      // 指定された月
      events.push(...getMonthlyEvents(mm.add(direction,'month'), this.props.birthday));
      // その先のnumヶ月
      if (num != 0) {
        for (let Inum = num; Inum != 0; Inum -= direction) {
          events.push(...getMonthlyEvents(mm.add(direction,'month'), this.props.birthday));
        }
      }
      this.props.setEvents(events);
  };
  onSwipeChange(x) {
      let mm = moment(this.props.cal_current);
      let next = '';
      if (x < 0) {
        next = mm.add(-1,'months').format('YYYY-MM-DD'); // 左側
      } else if (x > 0)  {
        next = mm.add(+1,'months').format('YYYY-MM-DD'); // 右側
      }
      this.setMonthEvents({ dateString: next }); // イベント設定
      this.props.changeMonth(next);
  };
  onSwipeLeft(gestureState) {
    console.log({myText: 'You swiped left!'});
    console.log(JSON.stringify(gestureState));
    this.onSwipeChange(1);
  }
  onSwipeRight(gestureState) {
    console.log({myText: 'You swiped right!'});
    console.log(JSON.stringify(gestureState));
    this.onSwipeChange(-1);
  }

  //componentDidUpdate () {
  //}
  render() {
    // 広告
    const _adMobBanner = BannerService().run(Setting.adUnitID_main); // main バナー
    const {navigate, cal_current,
           events, birthday, notif_flag,
           changeMonth} = this.props;
    // スタイル
    const customStyle = {
      hasEventCircle: {
        backgroundColor: 'powderblue'
      },
      hasEventDaySelectedCircle: {
        backgroundColor: 'powderblue'
      },
      currentDayCircle: {
        backgroundColor: null
      },
      selectedDayCircle: {
        backgroundColor: null
      },
      currentDayText: {
        color: 'black'
      },
      selectedDayText: {
        color: 'black'
      },
      //day: {
      //  height: 40
      //},
      weekRow: {
        height: 60
      },
      eventIndicator: {
        backgroundColor: 'transparent',
        width: 45,
        height: 20
      },
      eventText: {
        fontSize: 11,
        textAlign: 'center'
      },
      weekendHeading: {
        color: 'gray'
      },
      weekendDayText: {
        color: 'gray'
      }
    };
    //const event_dates = ['2017-05-01', '2017-05-02', '2017-05-13', '2017-05-14', '2017-05-31'];
    //const events = [
    //  {date: '2017-05-02', markFlag: true, text: '申'}
    //];
    I18n.translations = {
      'ja-JP': {
        title_in_locale: 'yyyy年M月'
      },
      'zh': {
        title_in_locale: 'yyyy年M月'
      },
      'en': {
        title_in_locale: 'MM yyyy'
      }
    };

    LocaleConfig.locales['ja-JP'] = {
      monthNames: ['１月','２月','３月','４月','５月','６月','７月','８月','９月','１０月','１１月','１２月'],
      monthNamesShort: ['１月','２月','３月','４月','５月','６月','７月','８月','９月','１０月','１１月','１２月'],
      dayNames: ['日', '月', '火', '水', '木', '金', '土'],
      dayNamesShort: ['日', '月', '火', '水', '木', '金', '土']
    };
    LocaleConfig.locales['zh'] = {
      monthNames: ['１月','２月','３月','４月','５月','６月','７月','８月','９月','１０月','１１月','１２月'],
      monthNamesShort: ['１月','２月','３月','４月','５月','６月','７月','８月','９月','１０月','１１月','１２月'],
      dayNames: ['日','一','二', '三','四','五','六'],
      dayNamesShort: ['日','一','二', '三','四','五','六']
    };
    
    LocaleConfig.defaultLocale = 'ja-JP';

    // マーク情報（evnetsから形式変換）
    let markedDates = {};
    events.forEach((event) => {
      if (event.markFlag) {
        markedDates[event.date] = { 'text':event.text, 'selected':true };
      } else {
        markedDates[event.date] = { 'text':event.text };
      }
    });
    // マーク下テキスト
    let get_mark_text = (date) => {
      if (!markedDates) {
        return '';
      }
      const dates = markedDates[date.dateString] || [];
      if (dates.text) {
        return dates.text;
      } else {
        return '';
      }
    };
    // マーク
    let get_date_marking = (date) => {
      if (!markedDates) {
        return false;
      }
      const dates = markedDates[date.dateString] || [];
      if (dates.selected) {
        return dates;
      } else {
        return false;
      }
    };
    // カレンダー移動時の処理（１ヶ月単位）
    let change_month = function (date) {
      this.setMonthEvents(date); // イベント設定
      changeMonth(date.dateString); // 月の変更
    };
    // 日を押されたら
    let onPressDay = function (date) {
      let next = 0;
      let mm = moment(date.dateString);
      if (mm.day() < 3) {
        // 週の前半なら前の月に移動
        next = mm.add(-1,'months').format('YYYY-MM-DD');
      } else if (mm.day() > 3) {
        // 週の後半なら前の月に移動
        next = mm.add(1,'months').format('YYYY-MM-DD');
      }
      if (next == 0) return false; // 終了
      this.setMonthEvents({ dateString: next }); // イベント設定
      changeMonth(next);
    };
    // レイアウト情報を取得
    let measureView = function (event) {
      let layout_info = {
        pos_x: event.nativeEvent.layout.x,
        pos_y: event.nativeEvent.layout.y,
        width: event.nativeEvent.layout.width,
        height: event.nativeEvent.layout.height
      };
      this.setState({
        layout_info: layout_info
      });
    };
    const gesture_config = {
      velocityThreshold: 0.01,
    };
    return (
  <View style={ styles.topContainer }>
    <ScrollView>
      { _adMobBanner }
      <GestureRecognizer
        onSwipeLeft={(st) => this.onSwipeLeft(st)}
        onSwipeRight={(st) => this.onSwipeRight(st)}
        config={ gesture_config }>
        <Calendar
          ref="calendar"
          markedDates={ markedDates } // 日付のマークとテキスト
          current={ cal_current }
          onDayPress={()=>{}}
          onDayLongPress={()=>{}}
          dayComponent={({date, state, marking}) => {
            return (<MyDay date={ date }
                           state={ state }
                           marking={ get_date_marking(date) }
                           onPress={ onPressDay.bind(this) }
                           onLongPress={ onPressDay.bind(this) }
                           theme={{
                             todayTextColor: 'red'
                           }}
                    >
                      { get_mark_text(date) /* markText マークテキスト */}
                    </MyDay>);
          }}
          //customStyle={customStyle}
          style={{ height:'100%' }}
          theme={{
            todayTextColor: 'red'
          }}
          calendarHeight={ 520 } // markTextの高さ分を増やす
          onMonthChange={ change_month.bind(this) } // for Calendar
          monthFormat={ I18n.t('title_in_locale') }
        />
      </GestureRecognizer>
    </ScrollView>
  </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    events: state.events,
    notif_flag: state.notif_flag,
    birthday: state.birthday,
    cal_current: state.cal_current,
    count: state.count
  };
}
function mapDispatchToProps(dispatch) {
  return {
    initEvents() {
      dispatch({ type:'INIT_EVENTS'});
    },
    changeMonth(date) {
      dispatch({ type:'CHANGE_MONTH', date: date });
    },
    setEvents(events) {
      dispatch({ type:'SET_EVENTS', events: events});
    },
    addEvents(events) {
      dispatch({ type:'ADD_EVENTS', events: events}); // 重複追加はしない
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)( LifeCalendarRX );
