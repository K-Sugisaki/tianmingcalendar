import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TextInput,
  Button,
  Switch,
  TouchableOpacity,
  StyleSheet,
  Linking,
} from 'react-native';
import moment from 'moment';
import { Constants } from 'expo';

// 多言語対応
import I18n from 'ex-react-native-i18n';
I18n.fallbacks = true;

// 天中殺と干支
import { UnmeiService } from './lifecalendar2/unmei';
const Unmei = UnmeiService();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 30,
    justifyContent: 'flex-start',
  },
  content_view: {
    margin: 10,
  },
});

// プライバシーポリシーへのリンクを開く
let gotoPrivacy = ()=> {
  let url = 'http://studio-kei.info/android/tianmingcalendar/privacy.html';
  Linking.openURL(url).catch(err => console.error('An error occurred', err));
};

export default class Menu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { manifest } = Constants;
    I18n.translations = {
      'ja-JP': {
        '設定': '設定',
        'プライバシーポリシー': 'プライバシーポリシー',
        'バージョン': 'バージョン'
      },
      'zh-CN': {
        '設定': '组态',
        'プライバシーポリシー': '隐私政策',
        'バージョン': '版本'
      },
      'zh': {
        '設定': '組態',
        'プライバシーポリシー': '隱私政策',
        'バージョン': '版本'
      },
      'en': {
        '設定': 'Config',
        'プライバシーポリシー': 'Privacy Policy',
        'バージョン': 'Version'
      }
    };
    return (
      <View style={styles.container}>
        {/* 設定 */}
        <View style={styles.content_view}>
          <Button title={ I18n.t('設定') }
            onPress={() => this.props.navigation.navigate('Profile')}
          />
        </View>
        {/* プライバシーポリシー */}
        <View style={styles.content_view}>
          <Button title={ I18n.t('プライバシーポリシー') }
            onPress={ gotoPrivacy }
          />
        </View>
        {/* バージョン */}
        <View style={styles.content_view}>
          <Text>
            { I18n.t('バージョン') } : { manifest.version }
          </Text>
        </View>
      </View>
    );
  }
}
