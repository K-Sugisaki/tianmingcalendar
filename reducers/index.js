// カレンダーのファンクション
import { getEvents, getUnmeiInfo, setNotif } from '../LifeCalendarRX';

// 永続化 Realm
//import Realm from 'realm';
let birthday = '2001-01-01'; // 初期値
let notif_flag = 0; // 初期値
//let realm = new Realm({
//  schema: [{name: 'Profile',
//    properties: {
//      birthday: 'string', // 誕生日
//      notif_flag: 'int' // 通知フラグ
//    }
//  }]
//});
import moment from 'moment';
//let profiles = realm.objects('Profile');
//if (profiles.length == 0) {
//  // 未設定なら初期値を設定
//  realm.write(() => {
//    realm.create('Profile', {birthday: birthday, notif_flag: notif_flag});
//  });
//}  
let profiles = [
  { birthday:'2001-01-01', notif_flag: 0}
];
birthday = profiles[0].birthday; // 誕生日を読み出す
notif_flag = profiles[0].notif_flag; // 通知フラグを読み出す

const initialState = {
  birthday: birthday,
  notif_flag: notif_flag,
  count: 0, // 使ってない
  cal_moment: moment().startOf('month'),
  cal_current: moment().startOf('month').format('YYYY-MM-DD'),
  events: [],
  unmei_info: []
};

export default function reducer(state = initialState, action = {}){
  switch(action.type){
  case 'INIT_EVENTS': {
    let events = getEvents(state.cal_moment, state.birthday);
    if (state.notif_flag) {
      setNotif(events); // 通知をスケジュール
    }
    return Object.assign({}, state, {
      events: events,
      unmei_info: getUnmeiInfo(state.birthday)
    });
  }
  case 'CHANGE_MONTH':
    return Object.assign({}, state, {
      cal_moment: moment(action.date),
      cal_current: action.date
    });
  case 'SET_EVENTS': {
    return Object.assign({}, state, {
      events: action.events
    });
  }
  case 'ADD_EVENTS': {
    // 重複追加はしない
    let event_dates = state.events.map((event) => event.date);
    let new_events = action.events.filter(event => {
      return !(event_dates.includes(event.date)); // 設定済のイベントは除く
    });
    return Object.assign({}, state, {
      events: [...state.events, ...new_events]
    });
  }
  case 'SET_BIRTHDAY': {
    // 永続化
//    realm.write(() => {
//      profiles[0].birthday = action.birthday; // 誕生日を書き込む
//    });
    let events = getEvents(state.cal_moment, action.birthday);
    // イベント通知をスケジュール
    if (state.notif_flag) {
      setNotif(events);
    }
    return Object.assign({}, state, {
      birthday: action.birthday,
      events: events,
      unmei_info: getUnmeiInfo(action.birthday)
    });
  }
  case 'SET_NOTIF_FLAG':
    // 永続化
//    realm.write(() => {
//      profiles[0].notif_flag = action.flag; // 設定を書き込む
//    });
    if (action.flag) {
      setNotif(state.events); // 通知をスケジュール
    }
    return Object.assign({}, state, {
      notif_flag: action.flag
    });
  case 'THIS_MONTH':
    // 今月
    let month = moment().startOf('month');
    return Object.assign({}, state, {
      cal_moment: month,
      cal_current: month.format('YYYY-MM-DD'),
      events: getEvents(month, state.birthday),
      count: 0
    });
  default:
    return state;
  };
};
