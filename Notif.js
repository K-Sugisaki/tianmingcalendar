// 通知
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';
import moment from 'moment';

/**
 * 通知サービス
 */
export function NotifService () {
  console.log('@@@ NotifService @@@');

  async function registerForPushNotificationsAsync() {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    console.log('////// status =>');
    console.log(status);
    if (status !== 'granted') {
      return;
    }
  };
  async function alertIfNotificationsDisabledAsync() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    if (status !== 'granted') {
      //alert('通知の権限を有効にする必要があります');
      console.log('////// 通知の権限を有効にする必要があります');
      registerForPushNotificationsAsync();
    } else {
      console.log('////// 通知権限有効 @@@');
    }
  }
  alertIfNotificationsDisabledAsync();
  
  return {
    // 通知をスケジュール
    setSchedule: function (title, message, date) {
      console.log('@@@ 通知スケジュール確認 @@@');
      console.log(title +'/'+ message);
      // 通知テスト
      //let test_date = moment();
      //test_date.add(10, 'seconds'); // 10 秒後
      //date = test_date; // テストのため上書き
      let schedule_time = moment(date).valueOf(); // Date に変換
      console.log(moment(schedule_time).format());
      Notifications.scheduleLocalNotificationAsync(
        {
          title: title,
          body: message,
        },
        {
          //repeat: 'minute',
          time: schedule_time,
        },
      );

    },
    // 通知をクリア
    cancelAll: function () {
      console.log('@@@ 通知をクリア @@@');
      Notifications.cancelAllScheduledNotificationsAsync();
    }
  };
}
