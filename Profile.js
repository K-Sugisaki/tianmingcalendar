import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TextInput,
  Button,
  Switch,
  TouchableOpacity,
  StyleSheet,
  DatePickerAndroid,
} from 'react-native';
import moment from 'moment';

// 多言語対応
import I18n from 'ex-react-native-i18n';
I18n.fallbacks = true;

// 天中殺と干支
import { UnmeiService } from './lifecalendar2/unmei';
const Unmei = UnmeiService();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 30,
    justifyContent: 'flex-start'
  },
  content_view: {
    flex: 1,
    justifyContent: 'flex-start'
  },
  border_view: {
    borderTopWidth: 1,
    borderTopColor: 'gray'
  },
  label_text: {
    margin: 10
  },
  end_view: {
    flex: 1,
    alignItems: 'flex-end'
  },
  birth_text: {
    fontSize: 20
  },
  info_view: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  info_label: {
    paddingLeft: 10,
    fontSize: 11
  },
  info_text: {
  },
  notif_switch: {
  }
});

class Profile extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { birthday, unmei_info, notif_flag, setBirthday, setNotifFlag } = this.props;
    let birthday_mmnt = moment(birthday);
    let config = {
      presetDate: new Date(birthday),
      simpleDate: new Date(birthday),
      spinnerDate: new Date(birthday),
      calendarDate: new Date(birthday),
      defaultDate: new Date(birthday),
      allDate: new Date(),
      maxDate: new Date(),
      simpleText: 'pick a date',
      spinnerText: 'pick a date',
      calendarText: 'pick a date',
      defaultText: 'pick a date',
      minText: 'pick a date, no earlier than today',
      maxText: 'pick a date, no later than today',
      presetText: 'pick a date, preset to 2020/5/5',
      allText: 'pick a date between 2020/5/1 and 2020/5/10'
    };
    let showPicker = async (stateKey, options) => {
      try {
        const {action, year, month, day} = await DatePickerAndroid.open(options);
        if (action === DatePickerAndroid.dismissedAction) {
          //
        } else {
          var date = new Date(year, month, day);
          var mmnt = moment(date);
          setBirthday(mmnt.format("YYYY-MM-DD"));
        }
      } catch ({code, message}) {
        console.warn(`Error in example '${stateKey}': `, message);
      }
    };
    I18n.translations = {
      'ja-JP': {
        birthday_label: '生年月日',
        notif_label:    '天中殺の前日に通知',
        type_label:     '天中殺の分類',
        month_label:    '天中殺の月',
        birthday: birthday_mmnt.format('YYYY年M月D日')
      },
      'zh-CN': {
        birthday_label: '生日',
        notif_label:    '不吉利的日子之前通知',
        type_label:     '不吉利的类型',
        month_label:    '月份不吉利',
        birthday: birthday_mmnt.format('YYYY年M月D日')
      },
      'zh': {
        birthday_label: '生日',
        notif_label:    '不吉利的日子之前通知',
        type_label:     '不吉利的類型',
        month_label:    '月份不吉利',
        birthday: birthday_mmnt.format('YYYY年M月D日')
      },
      'en': {
        birthday_label: 'Birthday',
        notif_label:    'Notify before the empty day.',
        type_label:     'Type of the empty day',
        month_label:    'Month of the empty',
        birthday: birthday_mmnt.format('MMMM Do YYYY')
      }
    };
    return (
      <View style={styles.container}>
        
        {/* 生年月日 */}
        <View style={styles.content_view}>
          <View style={styles.border_view}>
            <Text style={styles.label_text}>{ I18n.t('birthday_label') }</Text>
          </View>
          <View style={styles.end_view}>
            <Text style={styles.birth_text}
                //onPress={showPicker.bind(this, 'simple', {date: config.simpleDate})}
                //onPress={showPicker.bind(this, 'calendar', {date: config.calendarDate, mode: 'calendar'})}
                  onPress={showPicker.bind(this, 'spinner', {date: config.spinnerDate, mode: 'spinner'})}
                  >
              { I18n.t('birthday') }
            </Text>
          </View>
        </View>

        {/* 天中殺情報 */}
        <View style={styles.content_view}>
          <View style={styles.info_view}>
            <Text style={styles.info_label}>{ I18n.t('type_label') }</Text>
            <Text style={styles.info_text}>{ unmei_info.type }</Text>
          </View>
          <View style={styles.info_view}>
            <Text style={styles.info_label}>{ I18n.t('month_label') }</Text>
            <Text style={styles.info_text}>{ unmei_info.month }</Text>
          </View>
        </View>

        {/* 通知スイッチ */}
        <View style={styles.content_view}>
          <View style={styles.border_view}>
            <Text style={styles.label_text}>{ I18n.t('notif_label') }</Text>
          </View>
          <View style={styles.end_view}>
            <Switch style={styles.notif_switch}
              onValueChange={(value) => setNotifFlag(value?1:0)}
              value={notif_flag?true:false} />
          </View>
        </View>

        <View style={styles.content_view}>
          <View style={styles.border_view}>
          </View>
          {/* 空間 */}
        </View>
        <View style={styles.content_view}>
          {/* 空間 */}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    birthday: state.birthday,
    unmei_info: state.unmei_info,
    notif_flag: state.notif_flag
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setBirthday(birthday) {
      dispatch({ type:'SET_BIRTHDAY', birthday: birthday});
    },
    setNotifFlag(flag) {
      dispatch({ type:'SET_NOTIF_FLAG', flag: flag});
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)( Profile );
