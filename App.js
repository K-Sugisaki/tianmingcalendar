import React, { Component } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import moment from 'moment';
import {
  Text,
  Button,
} from 'react-native';

// ナビゲーター
import {
  createStackNavigator, createAppContainer
} from 'react-navigation';

// 永続化
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './configureStore'; // レデューサー読み込み
const { store, persistor } = configureStore();

// 多言語対応
import * as Localization from 'expo-localization';
import I18n from 'ex-react-native-i18n';
I18n.fallbacks = true;
I18n.locale = Localization.locale;

// 天中殺
import LifeCalendarRX from './LifeCalendarRX';

// 各ページ
import Menu from './Menu'; // メニュー
import Profile from './Profile'; // 設定

// 状態の変更リスナー
if (__DEV__) {
  console.log('@@@ デバッグ中です @@@');
  store.subscribe(() => {
    console.log('@@@ 状態の変更リスナー @@@');
    console.log('count=>'+ store.getState().count);
    console.log('birthday=>'+ store.getState().birthday);
    console.log('notif_flag=>'+ store.getState().notif_flag);
    console.log('count of events=>'+ store.getState().events.length);
  });
}

import Icon from 'react-native-vector-icons/FontAwesome';

I18n.translations = {
  'ja-JP': {
    menu_title: 'メニュー',
    home_title: '天命カレンダー',
    profile_title: '設定',
    date_format: 'M月D日'
  },
  'zh-CN': {
    menu_title: '菜单',
    home_title: '天命日历',
    profile_title: '组态',
    date_format: 'M月D日'
  },
  'zh': {
    menu_title: '菜單',
    home_title: '天命日曆',
    profile_title: '組態',
    date_format: 'M月D日'
  },
  'en': {
    menu_title: 'Menu',
    home_title: 'Tian Ming Calendar',
    profile_title: 'Config',
    date_format: 'MMMM D'
  }
};

class Home extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <LifeCalendarRX navigate={navigate} />
        </PersistGate>
      </Provider>
    );
  }
}

const MenuScreen = ({ navigation }) => (
  <Menu store={store} navigation={navigation} />
);
MenuScreen.navigationOptions = {
  title: I18n.t('menu_title')
};

const ProfileScreen = () => (
  <Profile store={store} />
);
ProfileScreen.navigationOptions = {
  title: I18n.t('profile_title')
};

const HomeScreen = ({ navigation }) => (
  <Home
    navigation={navigation}
  />
);
HomeScreen.navigationOptions = ({ navigation }) => {
  let headerRightStyle = {
    marginRight: 10,
    padding: 6,
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: 'gray',
  };
  return {
    title: I18n.t('home_title'),
    headerLeft: (
      // メニューアイコン
      <Icon name="bars" size={30} style={{marginLeft: 10}}
            onPress={() => navigation.navigate('Menu')}
            />
    ),
    headerRight: (
      // 今月に戻す
      <Text onPress={() => store.dispatch({ type:'THIS_MONTH' })}
            style={ headerRightStyle }>
        { moment().format(I18n.t('date_format')) }
      </Text>
    )
  };
};

const Stack = createStackNavigator(
  {
    Recent: { screen: HomeScreen },
    Menu: { screen: MenuScreen },
    Profile: { screen: ProfileScreen },
  },
  {
    initialRouteName: 'Recent'
  }
);
const AppContainer = createAppContainer(Stack);
export default class App extends React.Component {
  render() {
    return (
      <AppContainer
        ref={ nav => { this.navigator = nav; }}
      />
    );
  }
}
