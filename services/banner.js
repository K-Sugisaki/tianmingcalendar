/**
 *   バナーの生成
 *   import { BannerService } from '../services/banner';
 *   BannerService().run(adUnitID);
 */

import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { AdMobBanner } from 'expo-ads-admob';
import { Dimensions } from 'react-native'; 

/**
 * スマートバナーの高さは、画面の高さによって決まる
 */
function smartBannerHight(height) {
  if (height > 720) {
    return 90;
  } else if (height > 400) {
    return 50;
  } else {
    return 32;
  }
}
let { height, width } = Dimensions.get('window');
let bannerHeight = smartBannerHight(height) + 3 + 3;

// スタイル
const styles = StyleSheet.create({
  adMobBannerWrap: {
    zIndex: 1000,
    height: bannerHeight,
    paddingTop: 3,
    marginBottom: 3,
    backgroundColor: 'gray',
  },
  adMobBanner: {
    position: "absolute",
    top: 3
  },
});

export function BannerService () {
  return {
    run: function(adUnitID) {
      return (
        <View style={ styles.adMobBannerWrap }>
          <AdMobBanner
            bannerSize="smartBannerPortrait"
            style={ styles.adMobBanner }
            adUnitID={ adUnitID }
            testDeviceID="EMULATOR"
            onDidFailToReceiveAdWithError={ (error => console.log(error)) }
          />
        </View>
      );
    }
  };
}
